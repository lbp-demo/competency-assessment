# ---
# Load link tracking data from Rebrandly
# competency-assessment
# ---

source("scripts/create-libs.R")
source("scripts/create-paths.R")


clicks.filename <- "comms-channel-tracking.xlsx"
clicks.filepath <- file.path(data.repo, clicks.filename)

# Clicks ------------------------------------------------------------------

clicks.sheet <- "Clicks (Hours)"
clicks.filepath <- file.path(data.repo, clicks.filename)
clicks.raw <- read_xlsx(
  clicks.filepath, sheet = clicks.sheet
)

# > rm useless cols ####

clicks.temp00 <- clicks.raw %>% 
  select(-`Record timestamp (PHT)`, -Remarks) #  remove columns
  # select(Link, `Timestamp (UTC)`, Clicks, `Retro clicks`) # keep columns

# > Time to PHT ####

clicks.temp01 <- clicks.temp00 %>% 
  mutate(
    `Timestamp (PHT)` = with_tz(`Timestamp (UTC)`, tzone = "Asia/Manila")
  ) %>% 
  select(-`Timestamp (UTC)`)

# > Fill NA Clicks ####

clicks.temp02 <- clicks.temp01 %>% 
  replace_na(list(Clicks = 0, `Retro clicks` = 0))

# > Adjust Clicks with Retro clicks ####

clicks.temp03 <- clicks.temp02 %>% 
  mutate(
    `Total clicks` = Clicks + `Retro clicks`
  ) %>% 
  select(-Clicks, -`Retro clicks`)


# > Fill blank links ####

clicks.temp04 <- clicks.temp03 %>% 
  fill(Link, .direction = "down")

# out

clicks.df <- clicks.temp04
rm(clicks.raw)
rm(clicks.temp00)
rm(clicks.temp01)
rm(clicks.temp02)
rm(clicks.temp03)
rm(clicks.temp04)


# Channels ----------------------------------------------------------------

channels.sheet <- "channels"
channels.raw <- read_xlsx(
  clicks.filepath, sheet = channels.sheet,
  skip = 3,
  col_types = c(
    "text", # Channel
    "text", # Link
    "skip"  # Sent
  )
)

# out
channels.df <- channels.raw
rm(channels.raw)


# out ---------------------------------------------------------------------

clicks.outfile <- "link-tracking"
clicks.outpath <- file.path(data.cache, clicks.outfile)

# * RData ####
save(
  channels.df, clicks.df,
  file = paste0(clicks.outpath, ".RData")
)

# * xlsx ####
clicks.parsed <- list(
  "Channels" = channels.df,
  "Hourly clicks" = clicks.df
)
write_xlsx(
  clicks.parsed,
  paste0(clicks.outpath, ".xlsx")
)
